var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");

    var panel = this.nextElementSibling;
    if (panel.style.display === "flex") {
      panel.style.display = "none";
    } else {
      panel.style.display = "flex";
    }
  });
}

(function() {
  const sortList = document.querySelector('.sort-list');
  const sortBtn = document.querySelector('.sort');

  if(sortBtn) {
    sortBtn.addEventListener("click", function() {
        sortList.classList.toggle('none');
    });
  };
}());

(function() {
  const select = document.querySelector('.arr');
  const dropDown = document.querySelector('.pr-dropdown');

  if(select) {
    select.addEventListener("click", function() {
      dropDown.classList.toggle('none');
    });
  }
}());