(function(){
  const dragArea = document.getElementById('drag-area');

  const startUpload = function(files) {
    console.log(files);
  }

  dragArea.ondrop = function(e) {
    e.preventDefault();
    startUpload(e.dataTransfer.files);
  };

  dragArea.ondragover = function() {
    console.log('Dragging');
  };
}());


const loadFile = function(event) {
	const image = document.getElementById('output');
  image.src = URL.createObjectURL(event.target.files[0]);
  const svg = document.querySelector('#cloud');
  const Headtext = document.querySelector('.uploadcall');
  const ptext = document.querySelector('#drag-area p');
  const cancel = document.getElementById('cancel');
  svg.style.display = 'none';
  Headtext.style.display = 'none';
  ptext.style.display = 'none';

  if(cancel) {
    cancel.addEventListener('click', function(){
      image.removeAttribute('src');
      svg.style.display = 'initial';
      Headtext.style.display = 'block';
      ptext.style.display = 'block';
    })
  }
};

const loadFileUpload = function(event) {
	const img = document.getElementById('result');
  img.src = URL.createObjectURL(event.target.files[0]);
  const cloud = document.querySelector('#cloud-ddy');
  const Headtxt = document.querySelector('.uploadcta');
  const dy = document.getElementById('dyp');
  const remove = document.getElementById('remove');
  cloud.style.display = 'none';
  Headtxt.style.display = 'none';
  dy.style.display = 'none';

  if(remove) {
    remove.addEventListener('click', function(){
      img.removeAttribute('src');
      cloud.style.display = 'initial';
      Headtxt.style.display = 'block';
      dy.style.display = 'block';
    })
  }
};

const firstUpload = function(event) {
  const img = document.querySelector('.imag');
  const label = document.querySelector('.upload');
  img.src = URL.createObjectURL(event.target.files[0]);
  label.style.display = "none";
  img.style.display= "block";
}

const nxUpload = function(event) {
  const nx = document.querySelector('#nx');
  const nxl = document.querySelector('#nxl');
  nx.src = URL.createObjectURL(event.target.files[0]);
  nxl.style.display = "none";
  nx.style.display= "block";
}

const nsUpload = function(event) {
  const ns = document.querySelector('#ns');
  const nsl = document.querySelector('#nsl');
  ns.src = URL.createObjectURL(event.target.files[0]);
  nsl.style.display = "none";
  ns.style.display= "block";
}

const ncUpload = function(event) {
  const nc = document.querySelector('#nc');
  const nsl = document.querySelector('#ncl');
  nc.src = URL.createObjectURL(event.target.files[0]);
  ncl.style.display = "none";
  nc.style.display= "block";
}
