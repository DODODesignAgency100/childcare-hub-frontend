var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");

    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}

//Mobile Menu
const menu = document.getElementById('menu');
const bar = document.querySelector('.bar');
const closeMenu = document.querySelector('.bar-close');

if(bar) {
  bar.addEventListener('click', function() {
    if(menu.classList.contains('d-none')){
      menu.classList.remove('d-none');
    } 
  });
}

if(closeMenu) {
  closeMenu.addEventListener('click', function() {
    menu.classList.add('d-none');
});
}


// Modal
(function() {
const reserve = document.getElementById('reserve');
const reserveBtn = document.getElementById('resbtn');
  if(reserveBtn) {
    reserveBtn.addEventListener("click", function(){
      reserve.style.display = "block";
    
      document.onclick = function(e) {
        if(e.target == reserve) {
          reserve.style.display = "none";
        };
      };
    });
  }

}());

(function() {
const tour = document.getElementById('tour');
const tourBtn = document.getElementById('tourbtn');

  if(tourBtn) {
    tourBtn.addEventListener("click", function(){
      tour.style.display = "block";
    
      document.onclick = function(ev) {
        if(ev.target == tour) {
          tour.style.display = "none";
        };
      };
    });
  }

}());

(function() {
const report = document.getElementById('report');
const reportBtn = document.getElementById('reportbtn');

  if(reportBtn) {
    reportBtn.addEventListener("click", function(){
      report.style.display = "block";
    
      document.onclick = function(ev) {
        if(ev.target == report) {
          report.style.display = "none";
        };
      };
    });
  }

}());

(function() {
  const filterClose = document.getElementById('filterbtn');
  const filterForm = document.querySelector('.filter-prompt');
  const filterOpen = document.getElementById('filter');

  if(filterOpen) {
    filterOpen.addEventListener("click", function(){
      filterForm.style.display = "block";
    });
  }

  if(filterClose) {
    filterClose.addEventListener("click", function(){
      filterForm.style.display = "none";
    });
  }
}());

//Dynamically add more form input text type 

(function(){
  const licenceBtn = document.getElementById('more-li');
  const dayCareInfo = document.getElementById('daycareinfo');

  if(licenceBtn) {
    licenceBtn.addEventListener("click", function(){
      const newDiv = document.createElement('div');
      const newLabel = document.createElement('label');
      const newInput = document.createElement('input');

      newDiv.classList.add('form-group-up');
      newLabel.textContent = 'Daycare License Number';
      newLabel.setAttribute('for', 'daycarelicense');
      newInput.setAttribute('type', 'text');
      newInput.setAttribute('name', 'up-license');
      newInput.classList.add('form-control');

      newDiv.appendChild(newLabel);
      newDiv.appendChild(newInput);
      dayCareInfo.insertBefore(newDiv, licenceBtn);
    });
  }
}());

//Dynamically add more form input url type 

(function(){
  const linkBtn = document.getElementById('more-link');
  const timeSlots = document.getElementById('timeslots');

  if(linkBtn) {
    linkBtn.addEventListener("click", function(){
      const newDiv = document.createElement('div');
      const newLabel = document.createElement('label');
      const newInput = document.createElement('input');

      newDiv.classList.add('form-group-up');
      newLabel.textContent = 'Social Media Links';
      newLabel.setAttribute('for', 'Socialmedia');
      newInput.setAttribute('type', 'url');
      newInput.setAttribute('name', 'up-web');
      newInput.classList.add('form-control');

      newDiv.appendChild(newLabel);
      newDiv.appendChild(newInput);
      timeSlots.insertBefore(newDiv, linkBtn);
    });
  }
}());


// select option redirect
function link(src) {
  window.location= src
}

//form validation login 
//Parents Signin
(function() {
  const email = document.getElementById('pr-email');
  const password = document.getElementById('pr-pass');
  const errorText = document.querySelectorAll('.err-text');
  const form = document.querySelector('.regform');

  if(form) {
    form.addEventListener('submit', function(e) {
      e.preventDefault();
  
      if( email.value === '' || email.value > 1) {
        email.style.border = "1px solid #F21616";
        errorText[0].style.display = "block";
      }
  
      if( password.value === '' || password.value > 1) {
        password.style.border = "1px solid #F21616";
        errorText[1].style.display = "block";
      }
    })
  }
}());

//Landing page modal

(function(){
  const betaBtn = document.querySelector('.beta');
  const beta = document.querySelector('.modal-beta');
  const closeBtn = document.querySelector('.beta-close');

  if(betaBtn) {
    betaBtn.addEventListener('click', function(){
      beta.style.display = "block";
    })
  }

  if(closeBtn) {
    closeBtn.addEventListener('click', function(){
      beta.style.display = "none";
    })
  }
}())

//Parents Signup

// (function(){
//   const currentLocation = location.href;
//   const menuItems = document.querySelectorAll('.menu-list-light a');
//   menuItems.forEach(menu => {
//       if(menu.href === currentLocation) {
//           menu.className = 'active';
//           console.log(menu);
//       }
//   });
  
//   const currentLocation = location.href;
//   const menuItem = document.querySelectorAll('.menu-list-light a');
//   const menuLength = menuItem.length;
//   for (let i = 0; i < menuLength; i++) {
//     if(menuItem[i].href === currentLocation) {
//       menuItem[i].getAttribute("active");
//     }
//   }
// }());

// const currentLocation = location.href;
// const menuItem = document.querySelectorAll('a');
// const menuLength = menuItem.length;
// for (let i = 0; i < menuLength; i++) {
//   if(menu[i].href === currentLocation) {
//     menu[i].className = "active";
//   }
// }
